This tool was designed to locate and provide options as to how to parse the access-logs files of a domain, located within a cPanel server, users' home directory.

Provides options such as:

Locating and parsing only large logs, selecting individual logs, or parsing all logs.

Results output hanlding in either less, or sending the results to a pastebun and generating a link for easy access with the help of the internal 'hpaste' tool.

The function takes the role of a wrapper for the methods used to parse the logs. One such method being an in-house tool called "curdom" and the other method being a self developed one, comprised of multiple awk programs stringed together to provide similar and summarized results such as:

-Total number of hits per top visiting IP's

-Distribution of hits per hour for the specified time frame

-Reverse lookup of IP

-Count of top requested paths/files per IP